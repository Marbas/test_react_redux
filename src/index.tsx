import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { history } from "./store";

import "./index.css";
import store from "./store";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App history={history} />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
