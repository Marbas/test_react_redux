import React, { FC } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { History } from "history";

import Home from "./components/Home";
import Details from "./components/Details";

interface IAppProps {
  history: History;
}

const App: FC<IAppProps> = ({ history }) => {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact={true} path="/" component={Home} />
          <Route exact={true} path="/details/:id" component={Details} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
