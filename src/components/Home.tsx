import React, { FC, useRef, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import useIntersectionObserver from "../containers/hooks/useIntersectionObserver";

import {
  getPendingSelector,
  getManufacturersSelector,
  getErrorSelector,
} from "../store/manufacturers/selectors";
import { fetchManufacturersRequest } from "../store/manufacturers/actions";
import MenuIcon from "@material-ui/icons/Menu";
import Loader from "react-loader-spinner";
import {
  Container,
  Grid,
  Button,
  IconButton,
  Paper,
  Typography,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles({
  paper: {
    marginBottom: "10px",
    minWidth: "100%",
  },
  number: {
    minWidth: 50,
    padding: "0 20px",
  },
  id: {
    flex: 1,
  },
  name: {
    flex: 2,
  },
  country: {
    flex: 2,
  },
  button: {
    minWidth: 100,
  },
});

const Home: FC = () => {
  const dispatch = useDispatch();
  const pending = useSelector(getPendingSelector);
  const manufacturers = useSelector(getManufacturersSelector);
  const error = useSelector(getErrorSelector);
  const divRef = useRef<HTMLDivElement | null>(null);
  let history = useHistory();
  const classes = useStyles();

  const [isEndOfList] = useIntersectionObserver({
    elementRef: divRef,
  });

  useEffect(() => {
    isEndOfList && dispatch(fetchManufacturersRequest());
  }, [dispatch, isEndOfList]);

  return (
    <Container>
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        item
        xs
      >
        {manufacturers.Results.map(
          ({ Mfr_CommonName, Country, Mfr_ID }, index) => (
            <Paper className={classes.paper} key={Mfr_ID}>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                item
                xs
              >
                <Typography
                  className={classes.number}
                  variant="body2"
                  color="textSecondary"
                >
                  {++index}.
                </Typography>
                <Typography
                  className={classes.id}
                  variant="body2"
                  color="textSecondary"
                >
                  {Mfr_ID}
                </Typography>
                <Typography
                  className={classes.name}
                  variant="body2"
                  color="textSecondary"
                >
                  {Mfr_CommonName}
                </Typography>
                <Typography
                  className={classes.country}
                  variant="body2"
                  color="textSecondary"
                >
                  {Country}
                </Typography>

                <IconButton
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                  onClick={() => history.push(`details/${Mfr_ID}`)}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Paper>
          )
        )}

        {manufacturers.Count !== 0 && (
          <div ref={divRef}>
            {pending ? (
              <Loader color="#000" width={80} height={80} type="ThreeDots" />
            ) : error ? (
              <div>Error</div>
            ) : (
              <Button onClick={() => dispatch(fetchManufacturersRequest())}>
                add more
              </Button>
            )}
          </div>
        )}
      </Grid>
    </Container>
  );
};
export default Home;
