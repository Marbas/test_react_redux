import React, { FC, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getPendingSelector as getPendingMakesSelector,
  getMakesSelector,
  getErrorSelector as getErrorMakesSelector,
} from "../store/makes/selectors";
import { getModelsSelector } from "../store/models/selectors";
import { getManufacturersSelector } from "../store/manufacturers/selectors";
import { fetchMakesRequest } from "../store/makes/actions";
import { fetchModelsRequest } from "../store/models/actions";
import { match } from "../types";
import {
  Container,
  Grid,
  Typography,
  Button,
  Paper,
  makeStyles,
} from "@material-ui/core";
import { IResult } from "../store/manufacturers/types";

const useStyles = makeStyles({
  paper: {
    height: "100vh",
    padding: 20,
    overflow: "auto",
  },
  headr: {
    height: 50,
    margin: "20px 0",
    padding: 20,
  },
  makes: {
    flex: 1,
    paddingRight: 20,
  },
  models: {
    flex: 2,
  },
  id: {
    flex: 1,
    padding: "0 20px",
  },
  name: {
    flex: 2,
  },
  country: {
    flex: 2,
  },
  button: {
    minWidth: 100,
  },
  point: {
    cursor: "pointer",
  },
});

interface IMfcId {
  id: string;
}

interface IDetails {
  match: match<IMfcId>;
}

const Details: FC<IDetails> = ({
  match: {
    params: { id },
  },
}) => {
  const dispatch = useDispatch();
  const [manufacturer, setManufacturer] = useState<IResult>({
    Mfr_ID: 0,
    Mfr_CommonName: "-",
    Country: "-",
    Mfr_Name: "-",
  });
  const pendingMakes = useSelector(getPendingMakesSelector);
  const makes = useSelector(getMakesSelector);
  const errorMakes = useSelector(getErrorMakesSelector);
  const models = useSelector(getModelsSelector);
  const manufacturers = useSelector(getManufacturersSelector);
  const classes = useStyles();
  let history = useHistory();
  useEffect(() => {
    const currentManufacturer = manufacturers.Results.find(
      (item) => item.Mfr_ID.toString() === id
    );
    if (currentManufacturer) {
      setManufacturer(currentManufacturer);
      dispatch(fetchMakesRequest(currentManufacturer.Mfr_CommonName));
    }
  }, [dispatch, manufacturers, id]);

  useEffect(() => {
    makes.Results.length &&
      dispatch(fetchModelsRequest(makes.Results[0].Make_ID));
  }, [makes.Results, dispatch]);

  return (
    <Container>
      <Paper className={classes.headr}>
        <Grid
          container
          direction="row"
          justify="space-around"
          alignItems="baseline"
          item
          xs
        >
          <Button onClick={() => history.push("/")}>back</Button>
          <Typography
            className={classes.id}
            variant="body2"
            color="textSecondary"
          >
            {manufacturer.Mfr_ID}
          </Typography>
          <Typography
            className={classes.name}
            variant="body2"
            color="textSecondary"
          >
            {manufacturer.Mfr_CommonName}
          </Typography>
          <Typography
            className={classes.country}
            variant="body2"
            color="textSecondary"
          >
            {manufacturer.Country}
          </Typography>
        </Grid>
      </Paper>

      <Grid
        container
        direction="row"
        justify="center"
        alignItems="stretch"
        item
        xs
      >
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="stretch"
          item
          xs
          className={classes.makes}
        >
          <Paper className={classes.paper}>
            {pendingMakes ? (
              <div>Loading...</div>
            ) : errorMakes ? (
              <div>Error</div>
            ) : (
              makes.Results.map(({ Make_ID, Make_Name }, index) => (
                <div style={{ marginBottom: "10px" }} key={Make_ID}>
                  <Typography
                    className={classes.point}
                    variant="body2"
                    color="textPrimary"
                    onClick={() => dispatch(fetchModelsRequest(Make_ID))}
                  >
                    {++index}. {Make_Name}
                  </Typography>
                </div>
              ))
            )}
          </Paper>
        </Grid>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="stretch"
          item
          xs
          className={classes.models}
        >
          <Paper className={classes.paper}>
            {models.Results.map(({ Model_ID, Model_Name }, index) => (
              <div style={{ marginBottom: "10px" }} key={Model_ID}>
                <Typography variant="body2" color="textSecondary">
                  {Model_Name}
                </Typography>
              </div>
            ))}
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Details;
