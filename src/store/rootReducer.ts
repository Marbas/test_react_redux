import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import manufacturersReducer from "./manufacturers/reducer";
import makesReducer from "./makes/reducer";
import modelsReducer from "./models/reducer";

const rootReducer = combineReducers({
  manufacturers: manufacturersReducer,
  makes: makesReducer,
  models: modelsReducer,
  router: routerReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
