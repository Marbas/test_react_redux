import axios from "axios";
import { all, call, put, takeLatest } from "redux-saga/effects";

import { fetchModelsFailure, fetchModelsSuccess } from "./actions";
import { FETCH_MODELS_REQUEST } from "./actionTypes";
import { IModel, IResult, FetchModelsRequest } from "./types";

const getModels = (id: number) =>
  axios.get<IModel[]>(
    `${process.env.REACT_APP_API}/vehicles/GetModelsForMakeId/${id}?format=json`
  );

const clearData = (model: IModel): IModel => {
  if (model.Results)
    model.Results = model.Results.reduce((all: IResult[], curent: IResult) => {
      curent.Model_Name &&
        !all.some((item) => item.Model_ID === curent.Model_ID) &&
        all.push(curent);

      return all;
    }, []);
  return model;
};

function* fetchModelsSaga(action: FetchModelsRequest) {
  try {
    const response = yield call(getModels, action.id);
    yield put(
      fetchModelsSuccess({
        models: clearData(response.data),
      })
    );
  } catch (e) {
    yield put(
      fetchModelsFailure({
        error: e.message,
      })
    );
  }
}

function* modelsSaga() {
  yield all([takeLatest(FETCH_MODELS_REQUEST, fetchModelsSaga)]);
}

export default modelsSaga;
