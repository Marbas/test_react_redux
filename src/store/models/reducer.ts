import {
  FETCH_MODELS_REQUEST,
  FETCH_MODELS_SUCCESS,
  FETCH_MODELS_FAILURE,
} from "./actionTypes";

import { ModelsActions, ModelsState } from "./types";

const initialState: ModelsState = {
  pending: false,
  models: { Results: [] },
  error: null,
};

const modelsReducer = (state = initialState, action: ModelsActions) => {
  switch (action.type) {
    case FETCH_MODELS_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case FETCH_MODELS_SUCCESS:
      return {
        ...state,
        pending: false,
        models: {
          ...action.payload.models,
          Results: action.payload.models.Results,
        },
        error: null,
      };
    case FETCH_MODELS_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default modelsReducer;
