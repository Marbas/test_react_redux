import { createSelector } from "reselect";

import { AppState } from "../rootReducer";

const getPending = (state: AppState) => state.models.pending;

const getModels = (state: AppState) => state.models.models;

const getError = (state: AppState) => state.models.error;

export const getModelsSelector = createSelector(getModels, (models) => models);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error) => error);
