import {
  FETCH_MODELS_REQUEST,
  FETCH_MODELS_SUCCESS,
  FETCH_MODELS_FAILURE,
} from "./actionTypes";
import {
  FetchModelsRequest,
  FetchModelsSuccess,
  FetchModelsSuccessPayload,
  FetchModelsFailure,
  FetchModelsFailurePayload,
} from "./types";

export const fetchModelsRequest = (id: number): FetchModelsRequest => ({
  type: FETCH_MODELS_REQUEST,
  id,
});

export const fetchModelsSuccess = (
  payload: FetchModelsSuccessPayload
): FetchModelsSuccess => ({
  type: FETCH_MODELS_SUCCESS,
  payload,
});

export const fetchModelsFailure = (
  payload: FetchModelsFailurePayload
): FetchModelsFailure => ({
  type: FETCH_MODELS_FAILURE,
  payload,
});
