import {
  FETCH_MODELS_REQUEST,
  FETCH_MODELS_SUCCESS,
  FETCH_MODELS_FAILURE,
} from "./actionTypes";

export interface IModel {
  Count?: number;
  Message?: string;
  SearchCriteria?: string;
  Results: IResult[];
}

export interface IResult {
  Make_ID: number;
  Make_Name: string;
  Model_ID: number;
  Model_Name: string;
}

export interface ModelsState {
  pending: boolean;
  models: IModel;
  error: string | null;
}

export interface FetchModelsSuccessPayload {
  models: IModel;
}

export interface FetchModelsFailurePayload {
  error: string;
}

export interface FetchModelsRequest {
  type: typeof FETCH_MODELS_REQUEST;
  id: number;
}

export type FetchModelsSuccess = {
  type: typeof FETCH_MODELS_SUCCESS;
  payload: FetchModelsSuccessPayload;
};

export type FetchModelsFailure = {
  type: typeof FETCH_MODELS_FAILURE;
  payload: FetchModelsFailurePayload;
};

export type ModelsActions =
  | FetchModelsRequest
  | FetchModelsSuccess
  | FetchModelsFailure;
