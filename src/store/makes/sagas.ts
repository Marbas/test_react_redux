import axios from "axios";
import { all, call, put, takeLatest } from "redux-saga/effects";

import { fetchMakesFailure, fetchMakesSuccess } from "./actions";
import { FETCH_MAKES_REQUEST } from "./actionTypes";
import { IMake, IResult, FetchMakesRequest } from "./types";

const getMakes = (make: string) =>
  axios.get<IMake[]>(
    `${process.env.REACT_APP_API}/vehicles/GetMakeForManufacturer/${make}?format=json`
  );

const clearData = (make: IMake): IMake => {
  if (make.Results)
    make.Results = make.Results.reduce((all: IResult[], curent: IResult) => {
      curent.Make_Name &&
        !all.some((item) => item.Make_ID === curent.Make_ID) &&
        all.push(curent);

      return all;
    }, []);
  return make;
};

function* fetchMakesSaga(action: FetchMakesRequest) {
  try {
    const response = yield call(getMakes, action.make);
    yield put(
      fetchMakesSuccess({
        makes: clearData(response.data),
      })
    );
  } catch (e) {
    yield put(
      fetchMakesFailure({
        error: e.message,
      })
    );
  }
}

function* makesSaga() {
  yield all([takeLatest(FETCH_MAKES_REQUEST, fetchMakesSaga)]);
}

export default makesSaga;
