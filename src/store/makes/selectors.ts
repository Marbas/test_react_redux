import { createSelector } from "reselect";

import { AppState } from "../rootReducer";

const getPending = (state: AppState) => state.makes.pending;

const getMakes = (state: AppState) => state.makes.makes;

const getError = (state: AppState) => state.makes.error;

export const getMakesSelector = createSelector(getMakes, (makes) => makes);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error) => error);
