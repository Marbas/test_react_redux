import {
  FETCH_MAKES_REQUEST,
  FETCH_MAKES_SUCCESS,
  FETCH_MAKES_FAILURE,
} from "./actionTypes";

import { MakesActions, MakesState } from "./types";

const initialState: MakesState = {
  pending: false,
  makes: { Results: [] },
  error: null,
};

const makesReducer = (state = initialState, action: MakesActions) => {
  switch (action.type) {
    case FETCH_MAKES_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case FETCH_MAKES_SUCCESS:
      return {
        ...state,
        pending: false,
        makes: {
          ...action.payload.makes,
          Results: action.payload.makes.Results,
        },
        error: null,
      };
    case FETCH_MAKES_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default makesReducer;
