import {
  FETCH_MAKES_REQUEST,
  FETCH_MAKES_SUCCESS,
  FETCH_MAKES_FAILURE,
} from "./actionTypes";
import {
  FetchMakesRequest,
  FetchMakesSuccess,
  FetchMakesSuccessPayload,
  FetchMakesFailure,
  FetchMakesFailurePayload,
} from "./types";

export const fetchMakesRequest = (make: string): FetchMakesRequest => ({
  type: FETCH_MAKES_REQUEST,
  make,
});

export const fetchMakesSuccess = (
  payload: FetchMakesSuccessPayload
): FetchMakesSuccess => ({
  type: FETCH_MAKES_SUCCESS,
  payload,
});

export const fetchMakesFailure = (
  payload: FetchMakesFailurePayload
): FetchMakesFailure => ({
  type: FETCH_MAKES_FAILURE,
  payload,
});
