import {
  FETCH_MAKES_REQUEST,
  FETCH_MAKES_SUCCESS,
  FETCH_MAKES_FAILURE,
} from "./actionTypes";

export interface IMake {
  Count?: number;
  Message?: string;
  SearchCriteria?: string;
  Results: IResult[];
}

export interface IResult {
  Make_ID: number;
  Make_Name: string;
  Mfr_Name: string;
}

export interface MakesState {
  pending: boolean;
  makes: IMake;
  error: string | null;
}

export interface FetchMakesSuccessPayload {
  makes: IMake;
}

export interface FetchMakesFailurePayload {
  error: string;
}

export interface FetchMakesRequest {
  type: typeof FETCH_MAKES_REQUEST;
  make: string;
}

export type FetchMakesSuccess = {
  type: typeof FETCH_MAKES_SUCCESS;
  payload: FetchMakesSuccessPayload;
};

export type FetchMakesFailure = {
  type: typeof FETCH_MAKES_FAILURE;
  payload: FetchMakesFailurePayload;
};

export type MakesActions =
  | FetchMakesRequest
  | FetchMakesSuccess
  | FetchMakesFailure;
