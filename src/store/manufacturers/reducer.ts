import {
  FETCH_MANUFACTURERS_REQUEST,
  FETCH_MANUFACTURERS_SUCCESS,
  FETCH_MANUFACTURERS_FAILURE,
} from "./actionTypes";

import { ManufacturersActions, ManufacturersState } from "./types";

const initialState: ManufacturersState = {
  pending: false,
  manufacturers: { Results: [] },
  error: null,
  currentPage: 1,
};

const manufacturersReducer = (
  state = initialState,
  action: ManufacturersActions
) => {
  switch (action.type) {
    case FETCH_MANUFACTURERS_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case FETCH_MANUFACTURERS_SUCCESS:
      return {
        ...state,
        pending: false,
        manufacturers: {
          ...action.payload.manufacturers,
          Results: state.manufacturers.Results.concat(
            action.payload.manufacturers.Results
          ),
        },
        error: null,
        currentPage: state.currentPage + 1,
      };
    case FETCH_MANUFACTURERS_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default manufacturersReducer;
