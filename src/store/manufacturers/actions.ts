import {
  FETCH_MANUFACTURERS_REQUEST,
  FETCH_MANUFACTURERS_SUCCESS,
  FETCH_MANUFACTURERS_FAILURE,
} from "./actionTypes";
import {
  FetchManufacturersRequest,
  FetchManufacturersSuccess,
  FetchManufacturersSuccessPayload,
  FetchManufacturersFailure,
  FetchManufacturersFailurePayload,
} from "./types";

export const fetchManufacturersRequest = (): FetchManufacturersRequest => ({
  type: FETCH_MANUFACTURERS_REQUEST,
});

export const fetchManufacturersSuccess = (
  payload: FetchManufacturersSuccessPayload
): FetchManufacturersSuccess => ({
  type: FETCH_MANUFACTURERS_SUCCESS,
  payload,
});

export const fetchManufacturersFailure = (
  payload: FetchManufacturersFailurePayload
): FetchManufacturersFailure => ({
  type: FETCH_MANUFACTURERS_FAILURE,
  payload,
});
