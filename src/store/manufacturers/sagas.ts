import axios from "axios";
import { all, call, put, takeLatest, select } from "redux-saga/effects";

import {
  fetchManufacturersFailure,
  fetchManufacturersSuccess,
} from "./actions";
import { FETCH_MANUFACTURERS_REQUEST } from "./actionTypes";
import { IManufacturer, IResult } from "./types";
import { AppState } from "../rootReducer";

const getPage = (store: AppState): number => {
  return store.manufacturers.currentPage;
};

const getManufacturers = (currentPage: number) =>
  axios.get<IManufacturer[]>(
    `${process.env.REACT_APP_API}/vehicles/getallmanufacturers?format=json&page=${currentPage}`
  );

const clearData = (manufacturer: IManufacturer): IManufacturer => {
  if (manufacturer.Results)
    manufacturer.Results = manufacturer.Results.reduce(
      (all: IResult[], curent: IResult) => {
        curent.Mfr_CommonName &&
          !all.some((item) => item.Mfr_ID === curent.Mfr_ID) &&
          all.push(curent);

        return all;
      },
      []
    );
  return manufacturer;
};

function* fetchManufacturersSaga() {
  try {
    const currentPage = yield select(getPage);
    const response = yield call(getManufacturers, currentPage);
    yield put(
      fetchManufacturersSuccess({
        manufacturers: clearData(response.data),
      })
    );
  } catch (e) {
    yield put(
      fetchManufacturersFailure({
        error: e.message,
      })
    );
  }
}

function* manufacturersSaga() {
  yield all([takeLatest(FETCH_MANUFACTURERS_REQUEST, fetchManufacturersSaga)]);
}

export default manufacturersSaga;
