import { createSelector } from "reselect";

import { AppState } from "../rootReducer";

const getPending = (state: AppState) => state.manufacturers.pending;

const getManufacturers = (state: AppState) => state.manufacturers.manufacturers;

const getError = (state: AppState) => state.manufacturers.error;

const getPage = (state: AppState) => state.manufacturers.currentPage;

export const getManufacturersSelector = createSelector(
  getManufacturers,
  (manufacturers) => manufacturers
);

export const getPageSelector = createSelector(
  getPage,
  (currentPage) => currentPage
);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error) => error);
