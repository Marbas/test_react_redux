import {
  FETCH_MANUFACTURERS_REQUEST,
  FETCH_MANUFACTURERS_SUCCESS,
  FETCH_MANUFACTURERS_FAILURE,
} from "./actionTypes";

export interface IManufacturer {
  Count?: number;
  Message?: string;
  SearchCriteria?: string;
  Results: IResult[];
}

export interface IResult {
  Country: string;
  Mfr_CommonName: string;
  Mfr_ID: number;
  Mfr_Name: string;
}

export interface ManufacturersState {
  pending: boolean;
  manufacturers: IManufacturer;
  error: string | null;
  currentPage: number;
}

export interface FetchManufacturersSuccessPayload {
  manufacturers: IManufacturer;
}

export interface FetchManufacturersFailurePayload {
  error: string;
}

export interface FetchManufacturersRequest {
  type: typeof FETCH_MANUFACTURERS_REQUEST;
}

export type FetchManufacturersSuccess = {
  type: typeof FETCH_MANUFACTURERS_SUCCESS;
  payload: FetchManufacturersSuccessPayload;
};

export type FetchManufacturersFailure = {
  type: typeof FETCH_MANUFACTURERS_FAILURE;
  payload: FetchManufacturersFailurePayload;
};

export type ManufacturersActions =
  | FetchManufacturersRequest
  | FetchManufacturersSuccess
  | FetchManufacturersFailure;
