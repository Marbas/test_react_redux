import { all, fork } from "redux-saga/effects";

import manufacturersSaga from "./manufacturers/sagas";
import makesSaga from "./makes/sagas";
import modelsSaga from "./models/sagas";

export function* rootSaga() {
  yield all([fork(manufacturersSaga), fork(makesSaga), fork(modelsSaga)]);
}
